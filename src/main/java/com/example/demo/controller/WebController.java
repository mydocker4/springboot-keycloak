package com.example.demo.controller;

import com.example.demo.dao.WineryDAO;
import com.example.demo.entities.Winery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.security.Principal;
import java.util.ArrayList;

@Controller
public class WebController {

    //@Autowired
    private WineryDAO wineryDAO;

    @GetMapping(path = "/")
    public String index() {
        return "external";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) throws Exception {
        request.logout();
        return "redirect:/";
    }

    @GetMapping(path = "/wineries")
    public String wineries(Principal principal, Model model) {
        addWineries();
        Iterable<Winery> wineries = wineryDAO.findAll();
        model.addAttribute("wineries", wineries);
        model.addAttribute("username", principal.getName());

        return "wineries";
    }

    @GetMapping(path = "/hello")
    public String hello(Principal principal, Model model) {
        Winery winery = new Winery();
        model.addAttribute("wineries", new Winery[] {winery});
        model.addAttribute("username", principal.getName());

        return "hello";
    }

    // add wineries for demonstration
    public void addWineries() {

        Winery winery1 = new Winery();
        winery1.setAddress("1111 foo blvd");
        winery1.setName("Foo Industries");
        winery1.setServiceRendered("Important services");
        wineryDAO.save(winery1);

        Winery winery2 = new Winery();
        winery2.setAddress("2222 bar street");
        winery2.setName("Bar LLP");
        winery2.setServiceRendered("Important services");
        wineryDAO.save(winery2);

        Winery winery3 = new Winery();
        winery3.setAddress("33 main street");
        winery3.setName("Big LLC");
        winery3.setServiceRendered("Important services");
        wineryDAO.save(winery3);
    }

}
