package com.example.demo.dao;

import com.example.demo.entities.Winery;
import org.springframework.data.repository.CrudRepository;

public interface WineryDAO extends CrudRepository<Winery, Long> {

}
